

document.addEventListener('DOMContentLoaded', () => {
    let interrupteur = document.getElementsByTagName('img')[5]
    console.log(interrupteur)
    interrupteur.addEventListener('mousedown',function(e){
        console.log('ok')
        interrupteur.src = "Images/on.jpg"
        start()
    },{once : true})


    function start(){
        let firework1 = document.getElementsByTagName('img')[0]
        let firework2 = document.getElementsByTagName('img')[1]
        let firework3 = document.getElementsByTagName('img')[2]
        let firework4 = document.getElementsByTagName('img')[3]
        let firework5 = document.getElementsByTagName('img')[4]
        let interrupteur =document.getElementsByTagName('img')[5]
        setTimeout(function(){letItgo(firework3)},1000)
        setTimeout(function(){letItgo(firework2)},5000)
        setTimeout(function(){letItgo(firework4)},6000)
        setTimeout(function(){letItgo(firework1)},7000)
        setTimeout(function(){letItgo(firework5)},9000)
        setTimeout(function(){
            firework1.parentNode.removeChild(firework1);
            firework2.parentNode.removeChild(firework2);
            firework3.parentNode.removeChild(firework3);
            firework4.parentNode.removeChild(firework4);
            firework5.parentNode.removeChild(firework5);
            interrupteur.parentNode.removeChild(interrupteur);
            setTimeout(function(){Nyancat()},100)
    
        },10000)

    }

    function letItgo(element){
        element.classList.add("animation")

        setTimeout(function(){explode(element)},1000)
    }

    function explode(element){
        element.src = "Images/explosion.gif"
        var x = document.getElementById("myAudio"); 
        x.play()
        setTimeout(function(){element.src = "Images/null.png"},1000)
    }

    function Nyancat(){
        document.getElementsByTagName('body')[0].classList.add('nyan')
        var x = document.getElementById("nyanya"); 
        x.play()
        document.getElementsByTagName("p")[0].hidden =''; 
        document.getElementsByTagName("span")[0].hidden ='hidden';
        
    }

})